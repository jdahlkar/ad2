\documentclass[a4paper,12pt]{article}
\usepackage{fullpage}
\usepackage[british]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm} \newtheorem{theorem}{Theorem}
\usepackage{color}
\usepackage{float}
\usepackage{listings}
\lstset{% parameters for all code listings
	language=Python,
	frame=single,
	basicstyle=\small,  % nothing smaller than \footnotesize, please
	tabsize=2,
	numbers=left,
	framexleftmargin=2em,  % extend frame to include line numbers
	% xrightmargin=2em,  % extra space to fit 79 characters
	breaklines=true,
	breakatwhitespace=true,
	prebreak={/},
	captionpos=b,
	columns=fullflexible,
	escapeinside={\#*}{\^^M},
	mathescape
}
\usepackage{tikz} \usetikzlibrary{trees}
\usetikzlibrary{decorations.pathreplacing}
\usepackage{hyperref}  % should always be the last package

% useful colours (use sparingly!):
\newcommand{\blue}[1]{{\color{blue}#1}}
\newcommand{\green}[1]{{\color{green}#1}}
\newcommand{\red}[1]{{\color{red}#1}}

% useful wrappers for algorithmic/Python notation:
\newcommand{\length}[1]{\text{len}(#1)}
\newcommand{\twodots}{\mathinner{\ldotp\ldotp}}  % taken from clrscode3e.sty
\newcommand{\Oh}[1]{\mathcal{O}\left(#1\right)}

% useful (wrappers for) math symbols:
\newcommand{\Cardinality}[1]{\left\lvert#1\right\rvert}
%\newcommand{\Cardinality}[1]{\##1}
\newcommand{\Ceiling}[1]{\left\lceil#1\right\rceil}
\newcommand{\Floor}[1]{\left\lfloor#1\right\rfloor}
\newcommand{\Iff}{\Leftrightarrow}
\newcommand{\Implies}{\Rightarrow}
\newcommand{\Intersect}{\cap}
\newcommand{\Sequence}[1]{\left[#1\right]}
\newcommand{\Set}[1]{\left\{#1\right\}}
\newcommand{\SetComp}[2]{\Set{#1\SuchThat#2}}
\newcommand{\SuchThat}{\mid}
\newcommand{\Tuple}[1]{\langle#1\rangle}
\newcommand{\Union}{\cup}
\newcommand{\Returns}{\longrightarrow}

% fancy characters:
\usepackage{pifont}
%\newcommand{\scissors}{\ding{36}}
%\newcommand{\phone}{\ding{37}}
%\newcommand{\aircraft}{\ding{40}}
%\newcommand{\envelope}{\ding{41}}
\newcommand{\handpoint}{\ding{43}}
%\newcommand{\victory}{\ding{44}}
%\newcommand{\handwrite}{\ding{45}}
%\newcommand{\tick}{\ding{51}}
%\newcommand{\notick}{\ding{55}}

\title{\textbf{Algorithms \& Data Structures II (1DL231) \\
    Uppsala University -- Autumn 2013 \\
    Report for Assignment $1$  % replace n by 1, 2, or 3,
    by Team $8$}}              % replace t by your team number

\author{Johan DAHLKAR \and Michal MARCINIEWSKI}  % replace by your name(s)

%\date{Month Day, Year}
\date{\today}

\begin{document}

\maketitle

\section{Integer Sort}
\label{sect:isort}
This sorting algorithm relies on additional information given about the set to
be sorted; the contents interval. This allows us to sort data faster than for
an arbitrary set assuming the input data is of a constant size.

\subsection*{b. Worst-case Time Complexity Analysis}
\label{sect:pgm:isort}
\lstinputlisting[firstline=11,lastline=34,float=t,label=pgm:isort, caption={
Integer Sort implemented in Python.}]{integer_sort.py}

Note: All line references in this section, refer to listing~\ref{pgm:isort}.

The for-loop begining at line \ref{line:isort-for1} will run $|A|$ times and for
each iteration it will perform $\theta(1)$ work as all operations performed by 
the loop take constant time.

The second for-loop begining at line \ref{line:isort-for2} will run k times and
perform a \emph{total} of $k + |A| + \theta(1)$ work. This is because the
operations run in the while loop begining at line \ref{line:isort-while} will be
run exactly $|A|$ times since we can not have more elements in the sorted set
than there are elements present in $|A|$.

This yields a run-time complexity of $ 2\cdot|A| + k + \theta(1)$ which has the
tight bound $\theta(|A| + k)$. This is the best, worst and average run-time
complexity as the sorting of elements is completely independant of their initial
ordering.

\subsection*{c. Bounded Worst-case Time Complexity Analysis}
Assuming $k$ has an upper bound of $\Oh{|A|}$, as given in the assignment,
the worst-case run-time will be bounded by $\theta(|A|)$ as substituting
$k$ for $|A|$ in $\theta(|A| + k)$ results in $\theta(2\cdot|A|) = 
\theta(|A|)$. Thus the worst run-time complexity will be $\theta(|A|)$,
when applying the reasoning from \hyperref[sect:pgm:isort]{b}.
\pagebreak
\section{Binary Multiplication}
\label{sect:pgm:bmult}
\lstinputlisting[firstline=58,lastline=102,float=h,label=pgm:bmult,
caption={Binary Multiplication implemented in Python.}]{binary_mult.py}


\subsection*{b. Recursion Tree}
\label{sect:rectree}
\begin{figure}[t] % make it float to the top of a page
  \label{fig:tree}
  \begin{center}
    \begin{tikzpicture}
      [level 1/.style={sibling distance=15mm},
       level 2/.style={sibling distance=10mm},
       level 3/.style={sibling distance=5mm}]
      %\tikzstyle{every node}=[circle,draw]
      \node (root) {$n$}
      child{
        node{$n/2$}
      }
      child{
        node{$n/2$}
        child{node{$n/4$}
        child[dashed]{node(l){$\theta(1)$}}
        child[missing]
        child[missing]
        child[missing]}
        child{node{$n/4$}}
        child{node{$n/4$}}
        child{node{$n/4$}}
      }
      child{node{$n/2$}}
      child{node (r) {$n/2$}}
      ;
      \draw[
       decoration={
         brace,
         mirror,
         raise=5mm
       },
       decorate
      ] (l.west) -- (l.west -| r.east) node [midway, anchor = north,
      yshift=-5mm] {$n^2$};

      \draw[
       decoration={
         brace,
         raise=5mm
       },
       decorate
      ] (l.south) -- (l.south |- root.north) node [midway, anchor = east,
      xshift=-5mm] {$\lg(n)$};
    \end{tikzpicture}
    \caption{A recursion tree of the function $T(n) = 4 \cdot T(n/2) + n$.}
  \label{fig:trees}
  \end{center}
\end{figure}

The time complexity function $T(n)=4 \cdot T(n/2) + n$, can be derived
by analysing the code in listing \hyperref[pgm:bmult]{2}. For each recursive call
the problem is divided by two. For every call there are four recursive calls and
a merge step that takes linear time. Using the recursive tree method yields the
tree seen in figure 1.

The total time taken in order to multiply two binary numbers of length n
$= n(1+2+4+8+16+..) = \theta(n^2)$ (geometric series). Which means the
worst-case time complexity guess of $\theta(n^2)$ is reasonable to assume. 

\subsection*{c. Substitution Method}
Using the guess from b the assumption is made that $T(k) \leq c_1K^2 - c_2K$
for $k < n$.

$Base: T(n) = \theta(1)$ for all $n < n_0$, where $n_0=2$

\begin{proof}
\begin{align*}
T(n)&=4\cdot T(n/2)+n\\
&\leq 4(c_1(n/2)^2 - c_2(n/2)) + n\\
&=c_1n^2-2c_2n+n\\
&=c_1n^2-c_2n-(c_2-n)\\
&\leq c_1n^2-c_2n\ \ \ \ if\ c_2\geq 1\ and\ c_1\geq 3 
\end{align*}
\end{proof}

Which proves that the guess was correct.

\subsection*{d. Master Theorem}
The master theorem is applicable on this recurrence because it is of the form:

\vspace{12 pt}
$T(n)=aT(n/b)+f(n)$, \\
\vspace{12 pt}

and $4=a\geq 1$, $2=b\geq 1$ and $f(n) = n$ which is asymptotically positive.

\begin{proof}
\begin{align*}
&T(n)=4\cdot T(n/2)+n\\
&a=4,\ b=2\ \Rightarrow n^{\log_b a} =n^2;\ f(n)=n.\\
&CASE\ 1:\ f(n)=\Oh{n^{2-\epsilon}}\ for\ \epsilon =1.\\
&T(n)=\theta(n^2)
\end{align*}
\end{proof}

This is the same as the guess made in \hyperref[sect:rectree]{b}.

\pagebreak

\section{The Birthday Present Problem}
\label{sect:pgm:bp}
\lstinputlisting[firstline=11,lastline=60,float=t,label=pgm:bp, caption={
  Birthday Present Problem implemented in Python.}]{birthday_present.py}

\subsection*{a. Recursive Equation}
Starting with the recursive function $BP(P,t)$ (BP for Birthday Present).
$P$ is the set of prices, $x_1, ..., x_n$, $t$ is the target sum. Calling the
total sum of all prices $S$, initally $BP(P,t)$ returns false if $|P|=0$,
$t<0$ and $t>S$. 

\vspace{12 pt}
$Base:\ BP(1,t)= (x_1==t)$

\vspace{12 pt}
For $P>1$:\\
\vspace{12 pt}

$BP(P,t) = BP(P-1,t)\ or\ (x_p==t)\ or\ BP(P-1, t-x_p)$    for $0\leq t\leq S$
\footnote{See reference [2]} \\

\vspace{12 pt}

This function can be used to fill a 2D array of the size $t\cdot P$


\subsection*{d. Time Complexity}
The run time complexity of the birthday\_present function is $\theta(t \cdot n)$.
This is because the first for loop on line~\ref{line:bp-for1}, runs $\theta(1)$
work $n+1$ times. The second for loop on line~\ref{line:bp-for2}, runs $t$ times
and performs $\theta(n)$ work as the inner for loop on line~\ref{line:bp-for3},
runs $n+1$ times and performs $\theta(1)$ work. Putting these pieces together,
yields $\theta(n+1 + t \cdot n) = \theta(t \cdot n)$. This time complexity
applies to both the average and worst case.

The other function, generating a subset of P, runs in $\theta(n)$ in the worst
case. This is due to the function performing $\theta(1)$ work and running one
recursive call on $n-1$ until $n = 0$ in the worst case.

Putting the to functions together results in a worst case time complexity
of $\theta(t \cdot n + n) = \theta(t \cdot n)$.
 

\pagebreak
\pagebreak
\begin{thebibliography}{1}  % even better: use BibTeX!
\bibitem{CLRS3} Thomas H.~Cormen, Charles E.~Leiserson, Ronald
  L.~Rivest, and Clifford Stein.  \emph{Introduction to Algorithms}
  (third edition).  The MIT Press, 2009. Pages 1097-1100 and 1128-1134.
\bibitem{Wikipedia} \url{http://en.wikipedia.org/wiki/Subset_sum_problem#Pseudo-polynomial_time_dynamic_programming_solution}
\end{thebibliography}




% \subsection{Specification and Program}
% \label{sect:pgm:isort}

% \lstinputlisting[firstline=9,lastline=26,float=t,label=pgm:isort,caption={Python
%   implementation of the insertion sort algorithm on page~18 of
%   CLRS3~\cite{CLRS3}.  \handpoint\ Compare for instance
%   line~\ref{line:ins-sort-while} with line~5 of that algorithm: arrays
%   are indexed from~$1$ in CLRS3 but from~$0$ in Python.  Note also
%   that a \emph{closed} interval $\ell \twodots u$, as used in the
%   mathematical notation of the comments, is denoted by the
%   \emph{right-open} interval $\ell : u+1$ in Python; you can use the
%   Python notation in comments and the running text, as long as you
%   comply with the Python semantics.}]{isort.py}

% A specification of the sorting task and a Python implementation of our
% insertion-sort algorithm (taken from~\cite{CLRS3}) for this task are
% given in Listing~\ref{pgm:isort}.  We assume the elements to be sorted
% are integers, that they are provided in an array $A$ that is indexed
% from $0$ to $n-1$ (in the program, $n$ is referred to as
% $\length{A}$), that the sorting is to result in a non-decreasing order
% (that is, $A[0] \leq A[1] \leq \dots \leq A[n-1]$), and that the
% sorting is to be done \emph{in place}.  For brevity, we often refer to
% a sequence in non-decreasing order as a \emph{sorted} sequence.

% \subsection{Analysis}
% \label{sect:analysis:isort}

% Program~\ref{pgm:isort} has two nested loops, so we analyse it
% starting from the inner loop, in Lines~\ref{line:ins-sort-while}
% to~\ref{line:ins-sort-while-end}, whose purpose is to insert $A[j]$
% into the \emph{sorted} sequence $A[0 \twodots j-1]$, assuming $j>0$,
% yielding the sorted sequence $A[0 \twodots j]$.  Let
% $T_{\text{ins}}(j)$ denote the running time of this inner loop.  We
% get the following recurrence:
% \begin{equation} \label{rec:ins}
%   T_{\text{ins}}(j) =
%   \begin{cases}
%     \Theta(1) & \text{if~} A[j-1] \leq A[j] \hfill \text{(best case)} \\
%     \Theta(j) & \text{if~} A\left[\frac{j-1}{2}\right] < A[j] \leq
%     A\left[\frac{j+1}{2}\right] \hfill ~~ (\text{if~} j>1) ~~~ \text{(average case)} \\
%     \Theta(j) & \text{if~} A[j] < A[0] \hfill \text{(worst case)}
%   \end{cases}
% \end{equation}
% assuming that every comparison takes constant time and that every
% assignment takes constant time.

% We can now analyse the outer loop, and hence the whole algorithm.  Let
% $n$ denote $\length{A}$ and let $T(n)$ denote the running time of
% $\text{insertion\_sort}(A)$.  We get the following recurrence:
% \begin{equation*}
%   T(n) =
%   \begin{cases}
%     \Theta(1) & \text{if~} n < 2 \\
%     T(n-1) + T_{\text{ins}}(n) & \text{if~} n \geq 2
%   \end{cases}
% \end{equation*}
% Using recurrence~(\ref{rec:ins}), we get the following time complexity
% results:
% \begin{itemize}
% \item $T(n) = \Theta(n)$ in the \emph{best case}, where the array is
%   already non-decreasingly ordered before the sorting, so that
%   $T_{\text{ins}}(n) = \Theta(1)$ at \emph{every} iteration of the
%   outer loop, because $A[j]$ is always kept by the inner loop
%   \emph{behind} the sorted sequence $A[0 \twodots j-1]$.  This result
%   follows from Theorem~\ref{thm:ind} below, for the constants $a=1$
%   and $b=2$.
% \item $T(n) = \Theta(n^2)$ in the \emph{average case}, defined here as
%   follows: \emph{on average} over the iterations of the outer loop,
%   the inner loop inserts $A[j]$ into the \emph{middle} of the sorted
%   sequence $A[0 \twodots j-1]$, so that $T_{\text{ins}}(n) =
%   \Theta(n)$ on average at \emph{every} iteration of the outer loop.
%   This can be proven by induction: $\langle$insert your proof
%   here$\rangle$.
% \item $T(n) = \Theta(n^2)$ in the \emph{worst case}, where the array
%   is non-\emph{increasingly} ordered before the execution of the
%   algorithm, so that $T_{\text{ins}}(n) = \Theta(n)$ at \emph{every}
%   iteration of the outer loop, because $A[j]$ is always inserted by
%   the inner loop at the \emph{beginning} of the sorted sequence $A[0
%   \twodots j-1]$.  This result has the same proof as in the average
%   case above.
% \end{itemize}
% In conclusion, insertion sort takes $\Oh{n^2}$ time for an array of
% $n$ elements.

% \begin{theorem}
%   \label{thm:ind}
%   The following recurrence, for some \emph{constants} $a$ and $b$:
%   \[
%   T(n) =
%   \begin{cases}
%     \Theta(1) & \text{if~} n < b \\
%     a \cdot T(n-1) + \Theta(1) & \text{if~} n \geq b
%   \end{cases}
%   \]
%   has $\Theta(n)$ as closed form for $a = 1$, and $\Theta(a^n)$ as
%   closed form for $a > 1$.
% \end{theorem}

% \begin{proof}
%   By induction (left as an exercise to the reader in the AD1 course).
% \end{proof}

% \section*{Intellectual Property}

% We certify that the material in this report is solely produced by its
% authors, except where otherwise indicated and clearly referenced.

% \begin{thebibliography}{1}  % even better: use BibTeX!
% \bibitem{CLRS3} Thomas H.~Cormen, Charles E.~Leiserson, Ronald
%   L.~Rivest, and Clifford Stein.  \emph{Introduction to Algorithms}
%   (third edition).  The MIT Press, 2009.
% \end{thebibliography}

% \section{More \LaTeX\ and Technical Writing Advice}

% Unnumbered itemisation (only to be used when the order of the items
% does \emph{not} matter):\footnote{Use footnotes very sparingly, and
%   remember that footnote pointers are never preceded by a space and
%   always glued immediately \emph{behind} the punctuation, if there is
%   any.}
% \begin{itemize}
% \item Unnumbered displayed formula:
%   \[
%   E = m \cdot c^2
%   \]
% \item Numbered displayed formula (which is normally cross-referenced
%   somewhere):
%   \begin{equation}
%     \label{eq:emc2}
%     E = m \cdot c^2
%   \end{equation}
% \item Formula --- the same as formula~(\ref{eq:emc2}) --- spanning
%   more than one line:
%   \begin{gather*}
%     E \\ = \\ m \cdot c^2
%   \end{gather*}  
% \end{itemize}
% Numbered itemisation (only to be used when the order of the items
% \emph{does} matter):
% \begin{enumerate}
% \item First do this.
% \item\label{item:that} Then do that.
% \item If we are not finished, then go back to Step~\ref{item:that},
%   else stop.
% \end{enumerate}

% The typesetting of tables and elementary mathematics is exemplified in
% Table~\ref{tab:maths}; see
% \url{ftp://ftp.ams.org/pub/tex/doc/amsmath/short-math-guide.pdf} for
% many more details.  Do not use programming-language-specific notation
% (such as $!$ for negation, or $||$ for disjunction, or the equality
% sign $=$ for assignment) in mathematical formulas (but rather $\neg$
% or $\mathbf{not}$, and $\lor$ or $\mathbf{or}$, and $\gets$ or $:=$),
% as this testifies to a strong confusion of concepts.

% \begin{table}[t]  % make it float to the top of a page
%   \centering
%   \begin{tabular}{|r|l|c|}  % right left centre
%     \hline
%     Topic & \LaTeX\ code & Appearance \\ \hline
%     \hline
%     Greek letter & \verb|$\Theta,\Omega,\epsilon$| & $\Theta,\Omega,\epsilon$ \\ \hline
%     multiplication & \verb|$m \cdot n$| & $m \cdot n$ \\ \hline
%     division & \verb|$\frac{m}{n}, m \div n$| & $\frac{m}{n}, m \div n$ \\[+2pt] \hline
%     rounding down & \verb|$\lfloor n \rfloor$| & $\lfloor n \rfloor$ \\[+2pt] \hline
%     rounding up & \verb|$\lceil n \rceil$| & $\lceil n \rceil$ \\[+2pt] \hline
%     binary modulus & \verb|$m \bmod n$| & $m \bmod n$ \\ \hline
%     unary modulus & \verb|$m = n \mod \ell$| & $m = n \mod \ell$ \\ \hline
%     root & \verb|$\sqrt{n},\sqrt[3]{n}$| & $\sqrt{n},\sqrt[3]{n}$ \\ \hline
%     exponentiation, superscript & \verb|$n^{i}$| & $n^{i}$ \\ \hline
%     subscript & \verb|$n_{i}$| & $n_{i}$ \\ \hline
%     overline & \verb|$\overline{n}$| & $\overline{n}$ \\ \hline
%     base $2$ logarithm & \verb|$\lg n$| & $\lg n$ \\ \hline
%     base $b$ logarithm & \verb|$\log_b n$| & $\log_b n$ \\ \hline
%     binomial & \verb|$\binom{n}{k}$| & $\binom{n}{k}$ \\[+2pt] \hline
%     sum & \verb|\[ \sum_{i=1}^n i\]| & $\displaystyle\sum_{i=1}^n i$ \\ \hline
%     numeric comparison & \verb|$\leq,<,=,\neq,>,\geq$| & $\leq,<,=,\neq,>,\geq$ \\ \hline
%     non-numeric comparison & \verb|$\prec,\nprec,\preceq,\succeq$| & $\prec,\nprec,\preceq,\succeq$ \\ \hline
%     extremum & \verb|$\min,\max,+\infty,\bot,\top$| & $\min,\max,+\infty,\bot,\top$ \\ \hline
%     function & \verb|$f\colon A\to B,\circ,\mapsto$| & $f\colon A\to B,\circ,\mapsto$ \\ \hline
%     sequence, tuple & \verb|$\langle a,b,c \rangle$| & $\langle a,b,c \rangle$ \\ \hline
%     set & \verb|$\{a,b,c\},\emptyset,\mathbb{N}$| & $\{a,b,c\},\emptyset,\mathbb{N}$ \\ \hline
%     set membership & \verb|$\in,\not\in$| & $\in,\not\in$ \\ \hline
%     set comprehension & \verb|$\{i \mid 1 \leq i \leq n\}$| & $\{i \mid 1 \leq i \leq n\}$ \\ \hline
%     set operation & \verb|$\cup,\cap,\setminus,\times$| & $\cup,\cap,\setminus,\times$ \\ \hline
%     set comparison & \verb|$\subset,\subseteq,\not\supset$| & $\subset,\subseteq,\not\supset$ \\ \hline
%     logic quantifier & \verb|$\forall,\exists,\nexists$| & $\forall,\exists,\nexists$ \\ \hline
%     logic connective & \verb|$\land,\lor,\neg,\Rightarrow$| & $\land,\lor,\neg,\Rightarrow$ \\ \hline
%     logic & \verb|$\models,\equiv,\vdash$| & $\models,\equiv,\vdash$ \\ \hline
%     miscellaneous & \verb|$\&,\#,\approx,\sim,\ell$| & $\&,\#,\approx,\sim,\ell$ \\ \hline
%     dots & \verb|$\ldots,\cdots,\vdots,\ddots$| & $\ldots,\cdots,\vdots,\ddots$ \\ \hline
%     dots (context-sensitive) & \verb|$1,\dots,n; 1+\dots+n$| & $1,\dots,n; 1+\dots+n$ \\ \hline
%     parentheses (context-sensitive) & \verb|$\left(m^{n^k}\right),(m^{n^k})$| & $\left(m^{n^k}\right),(m^{n^k})$ \\[+2pt] \hline
%     identifier of $>1$ character & \verb|$\mathit{identifier}$| & $\mathit{identifier}$ \\ \hline
%     hyphen, \emph{n}-dash, \emph{m}-dash, minus & \texttt{-}, \texttt{--}, \texttt{---}, \verb|$-$| & -, --, ---, $-$ \\ \hline
% %    & \verb|$$| & $$ \\ \hline
%   \end{tabular}
%   \caption{The typesetting of elementary mathematics.  Note very carefully
%     when italics are used by \LaTeX\ and when not, as well as all the
%     horizontal and vertical spacing performed by \LaTeX.}
%   \label{tab:maths}
% \end{table}

% Figures can be imported with \verb|\includegraphics| (such as
% Figure~\ref{fig:isort}) or drawn inside the \LaTeX\ source code using
% the highly declarative notation of the \texttt{tikz} package (see
% Figure~\ref{fig:trees} for sample drawings).  It is perfectly
% acceptable in this course to include scans or photos of drawings that
% were carefully done by hand.

% \begin{figure}[t] % make it float to the top of a page
%   \begin{center}
%     \begin{tikzpicture}
%       [level 1/.style={sibling distance=30mm},
%        level 2/.style={sibling distance=15mm},
%        level 2/.style={sibling distance=10mm}]
%       \tikzstyle{every node}=[circle,draw]
%       \node{3}
%       child{
%         node{1}
%         child{node{0}}
%         child{node{2}}
%       }
%       child{
%         node{7}
%         child{
%           node{5}
%           child{node{4}}
%           child{node{6}}
%         }
%         child{node{8}}
%       };
%     \end{tikzpicture} \hspace{4mm}
%     \begin{tikzpicture}
%       [level 1/.style={sibling distance=30mm},
%        level 2/.style={sibling distance=15mm}]
%       \tikzstyle{every node}=[circle,draw]
%       \node{1}
%       child{
%         node{2}
%         child{node{8}}
%         child{node{6}}
%       }
%       child{
%         node{1}
%         child{node{6}}
%         child[missing]{node{k}}
%       }
%       ;
%     \end{tikzpicture} \hspace{4mm}
%     \begin{tikzpicture}[grow via three points={%
%         one child at (0,-1.5) and two children at (0,-1.5) and (-1.5,-1.5)}]
%       \tikzstyle{every node}=[circle,draw]
%       \node at (0,0) {6}
%       child{node{29}}
%       child{
%         node{14}
%         child{
%           node{38}
%         }
%       }
%       child{
%         node{8}
%         child{node{17}}
%         child{
%           node{11}
%           child{node{27}}
%         }
%       }
%       ;
%     \end{tikzpicture}
%   \end{center}
%   \caption{A binary search tree (on the left), a binary min-heap (in
%     the middle), and a binomial tree of rank $3$ (on the right).}
%   \label{fig:trees}
% \end{figure}

% If you are not sure whether you will stick to your current choice of
% notation, then introduce a new (possibly parametric) command.  For
% instance, upon
% \begin{center}
%   \verb|\newcommand{\Cardinality}[1]{\left\lvert#1\right\rvert}|
% \end{center}
% the in-lined formula \verb|$\Cardinality{S}$| typesets the cardinality
% of set $S$ as $\Cardinality{S}$ with size-adjusted vertical bars and
% proper spacing, but upon changing the definition of that parametric
% command to
% \begin{center}
%   \verb|\newcommand{\Cardinality}[1]{\# #1}|
% \end{center}
% and recompiling, the formula \verb|$\Cardinality{S}$| typesets the
% cardinality of set $S$ as $\#S$.
% %
% You can thus obtain an arbitrary number of changes in the document
% with a \emph{constant}-time change in its source code, rather than
% having to perform a \emph{linear}-time find-and-replace operation
% within the source code, which is painstaking and error-prone.  The
% source code of this document has some useful predefined commands about
% mathematics and algorithms.

% Use commands on positioning (such as \verb|\hspace|, \verb|\vspace|,
% and \verb|noindent|) and appearance (such as \verb|\small| for
% reducing the font size, and \verb|\textit| for italics) very
% sparingly, and ideally only in (parametric) commands, as the very idea
% of mark-up languages such as \LaTeX\ is to let the class designer
% (usually a trained professional typesetter) decide on where things
% appear and how they look.  For instance, \verb|\emph| (for emphasis)
% compiles (outside italicised environments, such as \texttt{theorem})
% into \textit{italics} under the \texttt{article} class used for this
% document, but it may compile into \textbf{boldface} under some other
% class.
% \begin{center}
%   \red{If you do not (need to) worry about \emph{how} things look, \\
%     then you can fully focus on \emph{what} you are trying to
%     express!}
% \end{center}

% Note that \emph{no} absolute numbers are used in the \LaTeX\ source
% code for any of the cross-references inside this document and even
% inside the included Python source code.  For ease of maintenance,
% \verb|\label| and \verb|\ref| are used for giving a label to something
% that is automatically numbered (such as an algorithm, equation,
% figure, footnote, item, line, section, subsection, or table) and
% referring to a label, respectively.  An item in a bibliography is
% labelled by \verb|\bibitem| and referred to by \verb|\cite| instead.
% Upon reshuffling the text, adding text, or deleting text, it suffices
% to recompile \emph{twice} in order to update all cross-references
% consistently.

% Prefer \verb|Section|$\sim$\verb|\ref{sect:isort}| over
% \verb|Section \ref{sect:isort}|, using the non-breaking space (given
% as $\sim$) instead of the space, giving ``Section~\ref{sect:isort}''
% instead of ``Section \ref{sect:isort}'' and thereby avoiding that a
% cross-reference is spread across a line break, as happened in the
% previous two lines: this is considered poor typesetting.

% \vfill

% \handpoint\ Feel free to report to the head teacher any other features
% that you would have liked to see discussed and exemplified in this
% template document.

\end{document}
