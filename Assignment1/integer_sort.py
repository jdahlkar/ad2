#!/usr/bin/env python2.7
'''
Assignment 1: Integer Sort

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''
import unittest

def integer_sort(A, k):
	'''
	Sig: int array[1..n], int -> int array[1..n]
	Pre: k >= 0, 
        A contains only integers 0 -> k
	Post: A, sorted in ascending order
	Example: integer_sort([5, 3, 6, 7, 12, 3, 6, 1, 4, 7]), 12) = 
				 [1, 3, 3, 4, 5, 6, 6, 7, 7, 12]
	'''
        l = len(A)
        Y = [0] * k
        ''' Var:  |A| - i '''
        for i in range(0, l): #* \label{line:isort-for1}
                x = A[i]
                Y[x-1] += 1  
        i = 0
        ''' Var:  k - j '''
        for j in range(0, k): #* \label{line:isort-for2}
                t = Y[j]
                ''' Var: t '''
                while t > 0: #* \label{line:isort-while}
                     A[i] = j+1
                     i += 1
                     t -= 1
                     
