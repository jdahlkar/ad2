#!/usr/bin/env python2.7
'''
Assignment 1: Binary Multiplication

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski

'''
import unittest

def binary_add(A, B):
        """
        Sig:	int[0..n-1], int[0..m-1] ==>
                    int[0..max(n,m)-1] || int[0..max(n,m)]
        Pre:    A and B contain only 0's and 1's.
        Post:   A + B where A and B represent binary numbers.
        """
        carry = 0
        n = len(A)
	m = len(B)
	i = n-1
        C = [0]*(max(n,m))
	padding = [0]*(max(n,m)-min(n,m))
	if(n > m):
		B = padding + B
                i = n - 1 
	elif(n < m):
		A = padding + A
                i = m - 1
        """Var:    i = max(|A|, |B|)  """
        while (i >= 0):
                if(A[i] == 1 and B[i] == 1):
                        C[i] = carry
                        carry = 1
                elif(A[i]==1 or B[i] == 1):
                        if(carry == 1):
                                C[i] = 0
                        else:
                                C[i] = 1
                else:
                        C[i] = carry
                        carry = 0
                i -= 1
	if(carry == 1):
                C = [carry] + C
        return C

def binary_shift_left(A, n):
        """
	Sig:	int[0..m-1], int ==> int[0..(m+n-1)]
	Pre:	n >= 0
	Post:	A with n appended 0's at the end.
	Example:   binary_shift_left([1,1], 3) = [1,1,0,0,0]
	"""
	return A + ([0]*n)

def binary_mult(A,B):
	"""
	Sig:	int[0..n-1], int[0..n-1] ==> int[0..2*n-1]
	Pre:	A and B only contains 0's and 1's and are of equal length.
                |A| and |B| > 0
	Post:	A and B multiplied as binary numbers.
	Var:	n, where n = |A|
	Example:	binary_mult([0,1,1],[1,0,0]) = [0,0,1,1,0,0]
	"""
        n = len(B)
        n2 = n*2
        if (n == 1):
		if(A[0] == 0 or B[0] == 0):
			return [0]
		else:
			return [1]

        if(n%2==1):
                A = [0] + A
                B = [0] + B
                n = len(B)

        AH = A[:n/2]
        AT = A[n/2:]
        BH = B[:n/2]
        BT = B[n/2:]

        P1 = binary_mult(AH, BH)
        P2 = binary_mult(AT, BT)
        P3 = binary_mult(AH, BT)
        P4 = binary_mult(BH, AT)

	X = binary_shift_left(P1, n)
	Y = binary_shift_left(P3, n/2)
        Z = binary_shift_left(P4, n/2)
        
	res = binary_add(binary_add(binary_add(Z, P2), Y), X)
        if (len(res) < n2):
                padding = n2 - len(res)
                return ([0]*padding)+res
        elif(len(res) > n2):
                remove = len(res) - n2
                return res[remove:]
        else:
                return res
