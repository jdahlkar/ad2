#!/usr/bin/env python2.7
'''
Assignment 1: Birthday Present

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''
import unittest

def birthday_present(P, n, t, A):
	'''
	Sig: int[0..n-1], int, int, bool[0..n][0..t] -> bool
	Pre: n, t >= 0, |A.x| = n+1, |A.y| = t+1, |P| = n 
	Post: Returns true if there is a subset of P, Ps, where sum(Ps) = t
              else false.
        Side effect: A is modified with booleans corresponding to the given
                     problem.
	Example: P = [2, 32, 234, 35, 12332, 1, 7, 56]
			 birthday_present(P, len(P), 299, A) = True
			 birthday_present(P, len(P), 11, A) = False
	'''
        ''' Var: n+1 - i'''
        for i in range(0,n+1): #* \label{line:bp-for1}
                A[i][0] = True
        ''' Var: t+1 - i'''
        for i in range(1,t+1): #* \label{line:bp-for2}
                ''' Var: n+1 - i'''
                for j in range(1, n+1): #* \label{line:bp-for3}
                        A[j][i] = A[j-1][i]
                        if(i >= P[j-1]):
                                A[j][i] = A[j][i] or A[j-1][i - P[j-1]]


        return A[n][t]
        


def birthday_present_subset(P, n, t, A):
	'''
	Sig: int[0..n-1], int, int, bool[0..n][0..t] -> int[0..z]
	Pre: n, t >= 0, |A.x| = n+1, |A.y| = t+1, |P| = n, A has the correct
        values for the given problem (run the birthay_present function first),
        P is equal to the P used to generate A. t is the same as the t used
        to generate A.
	Post: A subset of P whos sum is t if such a subset exists, else [].
        Var: n
	Example: P = [2, 32, 234, 35, 12332, 1, 7, 56]
		 birthday_present(P, n, 299, A)
		 birthday_present_subset(P, len(P), 299, A) = [56, 7, 234, 2]
	'''
        if(n == 0 or t == 0):
                return []
        if(t >= P[n-1]):
                if(A[n][t] and A[n-1][t-P[n-1]]):
                        return [P[n-1]]+birthday_present_subset(P,n-1,t-P[n-1],A)
                else:
                        return birthday_present_subset(P, n-1, t, A)
        else:
                return birthday_present_subset(P, n-1, t, A)
