#!/usr/bin/env python2.7
'''
Assignment 3: Controlling Maximum Flow

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''

import unittest
import networkx as nx
"""
Note that the unittest provided uses the function:
	networkx.ford_fulkerson(G,s,t)
You may use ford_fulkerson() to generate flows for any test networks you generate;
however, as always, your implementation of the senstive() function may NOT make use
of any of the graph algorithm implementations provided by networkx.
"""

# If your solution needs a queue (like the BFS algorithm), you can use this one:
from collections import deque

try:
        import matplotlib.pyplot as plt
        HAVE_PLT = True
except ImportError:
        HAVE_PLT = False


"""
F is represented in python as a dictionary of dictionaries; 
i.e., given two nodes u and v, the computed flow from u to v is given by F[u][v].
"""
def sensitive(G, s, t, F):
	"""
	Sig:    Graph(V,E), int, int, int matrix [0..|V|-1][0..|V|-1] $\longrightarrow$ int, int
	Pre:	G is a directed and connected networkx graph, contains no edges
                with negative wieghts and, s and t are nodes in G and there exists
                a path between them and they are not equal. 
                The in-degree of s is 0 and the out-degree of t is 0 and. F is
                a maximum flow matrix computed from G and index 
                accordingly to F[u][v] where u and v are nodes in G.
	Post:	Returns a edge in G between s and t that is sensitive.
	Ex:	   sensitive(G,0,5,F) $\longrightarrow$ (1, 3)
	"""
        
        Q = deque()
        Q.append(s)
	# Var: |G.nodes()| - index of u
        for u in G.nodes(): #* \label{line:sens:for1}
                G[u]['visited'] = False
        G[s]['visited'] = True
        # Var: |V| - |visited vertices|
        while Q: #* \label{line:sens:while}
                u = Q.pop()
                if(u == t):
                        break
                # Var: |G[u]| - index of v
                for v in G[u]: #* \label{line:sens:for2}
                        if v != 'visited':
                                residual = G[u][v]["capacity"] - F[u][v]
                                if G[v]['visited'] == False and residual > 0:
                                        G[v]['visited'] = True
                                        Q.append(v)
        # Var: |G| - index of u
        for u in G.nodes(): #* \label{line:sens:for3}
                if u != 'visited' and G[u]['visited'] :
                        # Var: |G[u]| - index of v
                        for v in G[u]: #* \label{line:sens:for4}
                                if v != 'visited' and  G[v]['visited'] == False:
					# Var: |G.nodes()| - index of w
					for w in G.nodes(): #* \label{line:sens:for5}
						del G[w]['visited']
                                        return u, v
        # Var: |G.nodes()| - index of v
	for v in G.nodes(): #* \label{line:sens:for6}
		del G[v]['visited']
	return None, None


class SensitiveSanityCheck(unittest.TestCase):
	"""
	Test suite for the sensitive edge problem
	"""
	def draw_graph(self, H, u, v, flow1, F1, flow2, F2):
		if not HAVE_PLT:
			return
		pos = nx.spring_layout(self.G)
		plt.subplot(1,2,1)
		plt.axis('off')
		nx.draw_networkx_nodes(self.G,pos)
		nx.draw_networkx_edges(self.G,pos)
		nx.draw_networkx_labels(self.G,pos)
		nx.draw_networkx_edge_labels(self.G,pos,edge_labels={(u,v):'{}/{}'.format(F1[u][v],self.G[u][v]['capacity']) for (u,v,data) in nx.to_edgelist(self.G)})
		plt.title('before: flow={}'.format(flow1))
		plt.subplot(1,2,2)
		plt.axis('off')
		nx.draw_networkx_nodes(self.G,pos)
		nx.draw_networkx_edges(self.G,pos)
		nx.draw_networkx_edges(self.G,pos,edgelist=[(u,v)],width=3.0,edge_color='b')
		nx.draw_networkx_labels(self.G,pos)
		nx.draw_networkx_edge_labels(self.G,pos,edge_labels={(u,v):'{}/{}'.format(F2[u][v],H[u][v]['capacity']) for (u,v,data) in nx.to_edgelist(self.G)})
		plt.title('after: flow={}'.format(flow2))
		
	def setUp(self):
		"""start every test with an empty directed graph"""
		self.G = nx.DiGraph()
		
	def run_test(self, s, t, draw=True):
		"""standard tests to run for all cases
		
		Makes use of the networkx ford flkerson implementation to generate the flow"""
		H = self.G.copy()
		flow_g, F_g = nx.ford_fulkerson(self.G, s, t)
		u,v = sensitive(self.G, s, t, F_g)
		self.assertIn(u, self.G, "Invalid edge ({}, {})".format(u ,v))
		self.assertIn(v, self.G[u], "Invalid edge ({}, {})".format(u ,v))
		H[u][v]["capacity"] = H[u][v]["capacity"] - 1
		flow_h, F_h = nx.ford_fulkerson(H, s, t)
		#if draw:
			#self.draw_graph(H, u, v, flow_g, F_g, flow_h, F_h)
		self.assertLess(flow_h, flow_g, "Returned non-sensitive edge ({},{})".format(u,v))
		
	def test_sanity(self):
		"""Sanity check"""
		# The attribute on each edge MUST be called "capacity"
		# (otherwise the Ford_Fulkerson algorithm below will fail).
		# self.G.add_edge(0, 1, capacity = 16)
		# self.G.add_edge(0, 2, capacity = 13)
		# self.G.add_edge(2, 1, capacity = 4)
		# self.G.add_edge(1, 3, capacity = 12)
		# self.G.add_edge(3, 2, capacity = 9)
		# self.G.add_edge(2, 4, capacity = 14)
		# self.G.add_edge(4, 3, capacity = 7)
		# self.G.add_edge(3, 5, capacity = 20)
		# self.G.add_edge(4, 5, capacity = 4)
                self.G.add_edge('a', 'b', capacity = 16)
                self.G.add_edge('a', 'c', capacity = 13)
                self.G.add_edge('c', 'b', capacity = 4)
                self.G.add_edge('a', 'd', capacity = 12)
                self.G.add_edge('d', 'c', capacity = 9)
                self.G.add_edge('c', 'e', capacity = 14)
                self.G.add_edge('e', 'd', capacity = 7)
                self.G.add_edge('d', 'f', capacity = 20)
                self.G.add_edge('e', 'f', capacity = 4)
		self.run_test('a','f',draw=True)
		
	@classmethod
	def tearDownClass(cls):
		if HAVE_PLT:
			plt.show()
	
if __name__ == "__main__":
	#unittest.main()
        # G = nx.DiGraph()
        # G.add_edge('a', 'b', capacity = 16)
        # G.add_edge('a', 'c', capacity = 13)
        # G.add_edge('c', 'b', capacity = 4)
        # G.add_edge('a', 'd', capacity = 12)
        # G.add_edge('d', 'c', capacity = 9)
        # G.add_edge('c', 'e', capacity = 14)
        # G.add_edge('e', 'd', capacity = 7)
        # G.add_edge('d', 'f', capacity = 20)
        # G.add_edge('e', 'f', capacity = 4)
        # flow_g, F_g = nx.ford_fulkerson(G, 'a', 'f')
        # for u in G.nodes():
        #         for v in G[u]:
        #                 print "(" + str(u) + ", "+ str(v) + ") = " + str(F_g[u][v])
        # print "Flow " + str(flow_g)
        # u, v = sensitive(G, 'a', 'f', F_g)
        # print u
        # print v
        # G[u][v]["capacity"] = G[u][v]["capacity"] - 1
        # flow_h, F_h = nx.ford_fulkerson(G, 'a', 'f')
        # print "New Flow " + str(flow_h)
	1+1
        
