#! /usr/bin/env python2.7

'''
Assignment 3: Reliable Communications

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''
import unittest
import sys
import math
import itertools
import heapq
"""HINT: 
You can find an example of an efficient priority queue
implementation in the section describing the heapq library of the
python manual [1]. Remember that you *should* avoid using global
variables. The recommended way to implement such a priority queue
would be to encapsulate the methods and the variables provided in the
example in a new class.

[1]: http://docs.python.org/2/library/heapq.html#priority-queue-implementation-notes
"""

import networkx as nx
"""IMPORTANT:
We're using networkx only to provide a reliable graph
object.	 Your solution may NOT rely on the networkx implementation of
any graph algorithms.  You can use the node/edge creation functions to
create test data, and you can access node lists, edge lists, adjacency
lists, etc. DO NOT turn in a solution that uses a networkx
implementation of a graph traversal algorithm, as doing so will result
in a score of 0.
"""



try:
	import matplotlib.pyplot as plt
	HAVE_PLT = True
except ImportError:
	HAVE_PLT = False


def reliable(G, s, t):
	"""
	Sig: graph G(V,E), vertex s, vertex t ==> list of nodes
	Pre: G is a valid undirected and connected networkx graph, containing no
	     self loops or negative weight edges. 
	     vertex s is not equal to vertex t.
	Post: Returns the most reliable path between s and t where the edge
	      weights are the percentage of failure along the path.
	Example: TestCase 1
	"""
	assert(s in G.nodes() and t in G.nodes())
	path = []
	dijkstra(G, s, t)
	cur = t
	# Var: The length of the path from cur to s
	while (cur != s): #* \label{line:rc:while}
		#path.append(cur)
		path = [cur] + path
		cur = G[cur]['parent']
	#path.append(s)
	path = [s] + path

        # Var: |G.nodes()| - index of v
        for v in G.nodes(): #* \label{line:rc:for1}
                del G[v]['visited']
                del G[v]['dist']
                del G[v]['parent']
                del G[v]['queued']

	return path

def dijkstra(G, s, t):
	"""
	Sig: graph G(V,E), vertex s, vertex t
	Pre: G is a valid undirected and connected networkx graph, containing no
	     self loops or negative weight edges.
	     vertex s is not equal to vertex t and exist in G
	Post: Runs a modified version of dijkstras single path algorithm and
	      modifies the vertices in G with distances from s, visited status,
	      and also presents a most reliable path from t to s via a vertices
	      'parent' attribute
	"""
	Q = []
	# Var: |G.nodes()| - index of v
 	for v in G.nodes(): #* \label{line:rcdijk:for1}
		G[v]['visited'] = False
		G[v]['dist'] = -1
		G[v]['parent'] = None
                G[v]['queued'] = False

	G[s]['dist'] = 0
	Q.append(s)
        G[s]['queued'] = True

	# Var: |V| - |visited nodes|
	while Q: #* \label{line:rcdijk:while}
		mindist = max
		u = None

		# Var: |Q| - index of v
		for v in Q: # Extract-Min #* \label{line:rcdijk:for2}
			if (G[v]['visited'] == False and G[v]['dist'] < mindist
			    and G[v]['dist'] >= 0):
				u = v
				if(u == t):
					return
		Q.remove(u)
                G[u]['queued'] = False
		G[u]['visited'] = True

		# Var: |G[u]| - index of v
		for v in G[u]: #* \label{line:rcdijk:for3}
			if (v != 'dist' and v != 'parent' and v !='visited'
                            and v != 'queued'):
				alt = 1 - (1-G[u]['dist'])*(1-G[u][v]['fp'])
				if (alt < G[v]['dist'] or G[v]['dist'] == -1): 
					G[v]['dist'] = alt # Decrease-Key
					G[v]['parent'] = u
					if (G[v]['visited'] == False and
                                            G[v]['queued'] != True):
                                                G[v]['queued'] = True
						Q.append(v)

class ReliableCommunicationsTest(unittest.TestCase):
    """Test suite for the reliable communications problem
    """
    def draw_mst(self, G, path, n):
        if not HAVE_PLT:
            return
        pos = nx.spring_layout(G) # positions for all nodes
        plt.subplot(120 + n)
        plt.title('Reliability %d' % n)
        plt.axis('off')
        # nodes
        nx.draw_networkx_nodes(G, pos, node_size = 700)
        # edges
        nx.draw_networkx_edges(G, pos, width = 6, alpha = 0.5, 
                               edge_color = 'b', style = 'dashed')
        from itertools import izip
        l = [(a, b) for a, b in izip(path[0:-1], path[1:])]
        T = nx.Graph()
        T.add_edges_from(l)
        nx.draw_networkx_edges(T, pos, width = 6)
        # labels
        nx.draw_networkx_labels(G, pos, font_size = 20, font_family = 'sans-serif')
    def test_sanity1(self):
        G = nx.Graph()
        G.add_edge('a', 'b', fp = 0.6)
        G.add_edge('a', 'c', fp = 0.2)
        G.add_edge('c', 'd', fp = 0.1)
        G.add_edge('c', 'e', fp = 0.7)
        G.add_edge('c', 'f', fp = 0.9)
        G.add_edge('a', 'd', fp = 0.3)
        path = reliable(G, 'b', 'f')
        self.assertEqual(path, ['b', 'a', 'c', 'f'], 'test 1 failed')
        self.draw_mst(G, path, 1)
        
    def test_sanity2(self):
        G = nx.Graph()
        G.add_edge('a', 'b', fp = 0.6)
        G.add_edge('a', 'c', fp = 0.9)
        G.add_edge('c', 'd', fp = 0.1)
        G.add_edge('c', 'e', fp = 0.7)
        G.add_edge('c', 'f', fp = 0.9)
        G.add_edge('a', 'd', fp = 0.3)
        path = reliable(G, 'b', 'f')
        self.assertEqual(path, ['b', 'a', 'd', 'c', 'f'], 'test 2 failed')
        self.draw_mst(G, path, 2)
    @classmethod
    def tearDownClass(cls):
        if HAVE_PLT:
            plt.show() # display
				
if __name__ == '__main__':
	# unittest.main()
	# G = nx.Graph()
	# G.add_edge('a', 'b', fp = 0.6)
	# G.add_edge('a', 'c', fp = 0.2)
	# G.add_edge('c', 'd', fp = 0.1)
	# G.add_edge('c', 'e', fp = 0.7)
	# G.add_edge('c', 'f', fp = 0.9)
	# G.add_edge('a', 'd', fp = 0.3)
	# path = reliable(G, 'b', 'f')
	# print path
        1+1
