#!/usr/bin/env python2.7
'''
Assignment 3: Party seating problem

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''
from collections import deque
import unittest

def party(known):
	"""
	Sig:	(int list[1..m])[1..n] $\longrightarrow$ boolean, int [1..j], int [1..k]
        Pre:    If known[x] contains y then known[y] contains x.
	Post:	Returns True of a seating arrangement is possible and two lists
                with the arrangements. Otherwise False and two empty 
                lists.
	Ex:		$[[1,2],[0],[0]] \longrightarrow True, [0], [1,2]$
	"""
        G = len(known)
        Color = [-1]*G
        Q = deque()
        # Var: G - j 
        for j in range(0,G): #* \label{line:party:for}
                if Color[j] == -1:
                        Color[j] = 0
                        Q.append(j)
                        #Var: |V| - |visited vertices|
                        while Q: #* \label{line:party:while}
                                u = Q.pop()
                                N = len(known[u])
                                #Var: N - i
                                for i in range(0,N): #* \label{line:party:for1}
                                        v = known[u][i]
                                        if known[u][i] and Color[v] == -1 :
                                                Color[v] = 1 - Color[u]
                                                #Q.append(v)
                                        elif known[u][i] and Color[v] == Color[u]:
                                                return False, [], []
        U = []
        V = []
        #Var: G - x
        for x in range(0,G): #* \label{line:party:for2}
                if Color[x] == 0:
                        U = U + [x]
                elif Color[x] == 1:
                        V = V + [x]
        return True, U, V


class PartySeatingTest(unittest.TestCase):
	"""Test suite for party seating problem
	"""
	
	def test_sanity(self):
		"""Sanity test
                
		A minimal test case.
		"""
		K = [[1,2],[0],[0]]
		(Found, A, B) = party(K)
		self.assertTrue((A == [0] and B == [1,2]) or (B == [0] and A == [1,2]))

if __name__ == '__main__':
	# unittest.main()
        # K = [[1,2],[0],[0],[4], [3]]
        # Found, A, B = party(K)
        # print Found
        # print A
        # print B
        1+1
