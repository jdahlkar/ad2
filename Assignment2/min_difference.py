#!/usr/bin/env python2.7
'''
Assignment 2: Search Term Replacement

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski 
'''
import unittest
# Sample matrix provided by us:
from string import ascii_lowercase

# Solution to part b:
def min_difference(u,r,R):
	"""
	Sig:	string, string, int[0..A, 0..A] ==> int
	Pre: All characters in u and r must exist in the resemblance matrix 
             R. 
	Post: Returns the minumum difference of u and r based on the contents
	      of R.	 
	Example: min_difference("dynamic","dinamck",R) ==> 3
	"""
	# To get the resemblance between two letters, use code like this:
	# difference = R['a']['b']
        n = len(u)
        m = len(r)
        diff = [[0 for i in range(n+1)] for j in range(m+1)]
        diff[0][0] = R['-']['-']
        #Var: n - i
        for i in range(1, n+1):
                diff[0][i]= diff[0][i-1] + R[u[i-1]]['-']
        #Var: m - j
        for j in range(1, m+1):
                diff[j][0]= diff[j-1][0] + R['-'][r[j-1]]
        #Var: n - i
        for i in range(1, n+1):
                #Var: m - j
                for j in range(1, m+1):
                        diff[j][i] = min(diff[j][i-1] + R[u[i-1]]['-'], 
                                         diff[j-1][i] + R['-'][r[j-1]],
                                         diff[j-1][i-1] + R[u[i-1]][r[j-1]])
        return diff[m][n]
        
# Solution to part c:
def min_difference_align(u,r,R):
	"""
	Sig:	string, string, int[0..|A|, 0..|A|] ==> int, string, string
	Pre: All characters in u and r must exist in the resemblance matrix 
             R. 
	Post: Returns the minimum difference of u and r based on the contents
	      of R, and a optimal
              alignment according to this minimum difference. 
	Example: min_difference_align("dynamic","dinamck",R) ==> 3,
                                                     "dynamic-", "dinamc-k"
	"""
        align_u = ""
        align_r = ""
        n = len(u)
        m = len(r)
        diff = [[0 for i in range(n+1)] for j in range(m+1)]
        diff[0][0] = R['-']['-']
        #Var: n - i
        for i in range(1, n+1): #* \label{line:md-for1}
                diff[0][i]= diff[0][i-1] + R[u[i-1]]['-']
        #Var: m - j
        for j in range(1, m+1): #* \label{line:md-for2}
                diff[j][0]= diff[j-1][0] + R['-'][r[j-1]]
        #Var: n - i
        for i in range(1, n+1): #* \label{line:md-for3}
                #Var: m - j
                for j in range(1, m+1): #* \label{line:md-for4}
                        diff[j][i] = min(diff[j][i-1] + R[u[i-1]]['-'], 
                                         diff[j-1][i] + R['-'][r[j-1]],
                                         diff[j-1][i-1] + R[u[i-1]][r[j-1]])
        i = n
        j = m
        #Var: i | j
        while(i > 0 or j > 0): #* \label{line:md-while1}
                if(i > 0 and j > 0 and diff[j][i] == diff[j-1][i-1] + 
                   R[u[i-1]][r[j-1]]):
                        align_u = u[i-1] + align_u
                        align_r = r[j-1] + align_r
                        i -= 1
                        j -= 1
                elif(j > 0 and diff[j][i] == diff[j-1][i] + R['-'][r[j-1]]):
                        align_u = '-' + align_u
                        align_r = r[j-1] + align_r
                        j -= 1
                elif(i > 0 and diff[j][i] == diff[j][i-1] + R[u[i-1]]['-']):
                        align_u = u[i-1] + align_u
                        align_r = '-' + align_r
                        i -= 1
        return diff[m][n], align_u, align_r
        

class MinDifferenceTest(unittest.TestCase):
	"""Test Suite for search term replacement problem
	
	Any method named "test_something" will be run when this file is executed.
	Use the sanity check as a template for adding your own test cases if you wish.
	(You may delete this class from your submitted solution.)
	"""
	
	def test_diff_sanity(self):
		"""Minimum difference sanity test
		
		Given a simple resemblance matrix, test that the reported difference
                is the expected minimum.
		Do NOT assume we will always use this resemblance matrix when testing!
		"""
		alphabet = ascii_lowercase + '-'
		R = dict( [ ( a, dict( [ ( b, (0 if a==b else 1) ) for b in alphabet ] ) ) for a in alphabet ] )
		self.assertEqual(min_difference("dynamic","dinamck",R),3)

	def test_align_sanity(self):
		"""Simple alignment
		
		Passes if the returned alignment matches the expected one.
		"""
		alphabet = ascii_lowercase + '-'
		R = dict( [ ( a, dict( [ ( b, (0 if a==b else 1) ) for b in alphabet ] ) ) for a in alphabet ] )
		diff, u, r = min_difference_align("polynomial", "exponential", R)
		self.assertEqual(diff, 6)
		self.assertEqual(u,'--polynomial')
		self.assertEqual(r,'exponen-tial')

if __name__ == '__main__':
	#unittest.main()
        #alphabet = ascii_lowercase + '-'
        #R = dict( [ ( a, dict( [ ( b, (0 if a==b else 1) ) for b in alphabet ] ) )
        #            for a in alphabet ] )
        #diff, u, r = min_difference_align("polynomial", "exponential", R)
        #print str(diff)
        #print u
        #print r
        
