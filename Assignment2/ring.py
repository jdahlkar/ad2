#!/usr/bin/env python2.7
'''
Assignment 2: Ring Detection

AD2, HT13
Team Number: 8
Student Names: Johan Dahlkar, Michal Marciniewski
'''
import unittest
import networkx as nx
"""IMPORTANT:
We're using networkx only to provide a reliable graph
object.	 Your solution may NOT rely on the networkx implementation of
any graph algorithms.  You can use the node/edge creation functions to
create test data, and you can access node lists, edge lists, adjacency
lists, etc. DO NOT turn in a solution that uses a networkx
implementation of a graph traversal algorithm, as doing so will result
in a score of 0.
"""
try:
	import matplotlib.pyplot as plt
	HAVE_PLT = True
except ImportError:
	HAVE_PLT = False

def ring(G):
	"""
	Sig: graph G(node,edge) ==> boolean
	Pre: G is a valid undirected networkx graph and contains no self loops.
	Post: True if there is a ring/cycle in the graph, else False.
	Example: 
		ring(g1) ==> False
		ring(g2) ==> True
	"""
        v = G.nodes()
        res = False
        """ Var: |v| - index of i """
        for i in v:
                G[i]['visited'] = False
        """ Var: |v| - index of i """
        for i in v:
                if (G[i]['visited'] == False):
                        if(ring_sub(G, i, i) == True):
                                res = True
                                break
        """ Var: |v| - index of i """
        for i in v:
                del G[i]['visited']
        return res

def ring_sub(G, v, parent):
        """
	Sig: graph G(node,edge), node, node ==> boolean
	Pre: G is a valid undirected networkx graph and contains no self loops.
             v and parent are valid nodes and parent is initially equal to v 
             itself.
	Post: True if there is a ring/cycle in the graph, else False.
	Example: 
		ring_sub(g1, g1[0], g1[0]) ==> False
		ring_sub(g2, g2[0], g2[0]) ==> True
	Var: Number of unvisited nodes connected to v
	"""
        if(G[v]['visited'] == True):
                return True
        G[v]['visited'] = True
        nbrs = G[v]
        """ Var: |nbrs| - (index of i) """
        for i in nbrs:
                if(i != parent and i != 'visited'):
                        if(ring_sub(G, i, v) == True):
                                return True
        return False

def ring_extended(G):
	"""
	Sig: graph G(node,edge) ==> boolean, int[0..j-1]
	Pre: G is a valid undirected networkx graph and contains no self loops.
	Post: Returns True if a cycle/ring exists in the graph along with the
              cycle, else False and an empty list (as no nodes are present in a
              cycle.
	Example: 
		ring_extended(g1) ==> False, []
		ring_extended(g2) ==>  True, [3,7,8,6,3]
	"""
        v = G.nodes()
        path = []
        res = False
        """ Var: |v| - index of i """
        for i in v: #* \label{line:re-for1}
                G[i]['visited'] = False
        """ Var: |v| - i """
        for i in v: #* \label{line:re-for2}
                if (G[i]['visited'] != True):
                        if(ring_extended_sub(G, i, i, path) == True):
                                res = True
                                break
        """ Var: |v| - index of i """
        for i in v: #* \label{line:re-for3}
                del G[i]['visited']
        if (res == False):
                return False, []
        cycle = []
        l = len(path)
        cycle.append(path[0])
        """ Var: l - i """
        for i in range(1, l): #* \label{line:re-for4}
                cycle.append(path[i])
                if(path[i] == path[0]):
                        break
        return res, cycle

def ring_extended_sub(G, v, parent, path):
        """
	Sig: graph G(node,edge), node, node ==> boolean
	Pre: G is a valid undirected networkx graph and contains no self loops.
             v and parent are valid nodes and parent is initially equal to v 
             itself.
	Post: True if there is a ring/cycle in the graph, else False. Also
              updates path to contain the path that lead from the initial node
              v to the complete cycle.
	Var: Number of unvisited nodes connected to v
	Example: 
		ring_extended_sub(g1, g1[0], g1[0]) ==> False
		ring_extended_sub(g2, g2[0], g2[0]) ==> True
	"""
        if(G[v]['visited'] == True):
                path.append(v)
                return True
        G[v]['visited'] = True
        nbrs = G[v]
        """ Var: |nbrs| - (index of i) """
        for i in nbrs:
                if(i != parent and i != 'visited'):
                        if(ring_extended_sub(G, i, v, path) == True):
                                path.append(v)
                                return True
        return False

	
def draw_graph(G,r):
	"""Draw graph and the detected ring
	"""
	if not HAVE_PLT:
		return
	pos = nx.spring_layout(G)
	plt.axis('off')
	nx.draw_networkx_nodes(G,pos)
	nx.draw_networkx_edges(G,pos,style='dotted') # graph edges drawn with dotted lines
	nx.draw_networkx_labels(G,pos)
	
	# add solid edges for the detected ring
	if len(r) > 0:
		T = nx.Graph()
		T.add_path(r)
		for (a,b) in T.edges():
			if G.has_edge(a,b):
				T.edge[a][b]['color']='g' # green edges appear in both ring and graph
			else:
				T.edge[a][b]['color']='r' # red edges are in the ring, but not in the graph
		nx.draw_networkx_edges(T,pos, edge_color=[edata['color'] for (a,b,edata) in T.edges(data=True)], width=4)
	plt.show()
	
class RingTest(unittest.TestCase):
	"""Test Suite for ring detection problem
	"""
	
	def test_sanity(self):
		"""Simple positive and negative test
		
		Find a ring in a graph that has one,
		don't find a ring in a graph that doesn't.
		"""
		testgraph = nx.Graph([(0,1),(0,2),(0,3),(2,4),(2,5),(3,6),(3,7),(7,8)])
		self.assertFalse(ring(testgraph))
		testgraph.add_edge(6,8)
		self.assertTrue(ring(testgraph))
		
	def test_extended_sanity(self):
		"""Return nodes that form a ring
		
		Given a graph that has exactly one ring, correctly return a list of nodes
		representing a ring traversal.
		"""
		testgraph = nx.Graph([(0,1),(0,2),(0,3),(2,4),(2,5),(3,6),(3,7),(7,8),(6,8)])
		found, thering = ring_extended(testgraph)
		self.assertTrue(found)
		self.assertItemsEqual(thering, [3,6,7,8,3])
		# Uncomment to visualize the graph and returned ring:
		draw_graph(testgraph,thering)

if __name__ == '__main__':
	#unittest.main()
        
